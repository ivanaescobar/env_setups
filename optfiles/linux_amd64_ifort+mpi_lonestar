#!/bin/bash
#  
# For running on TACC's Lonestar 5 
#  Compiler optimizations for Intel E5-2690 (Haswell) Processors
#
# Note: be sure to load the following modules
#
#  module load netcdf
#  module load parallel-netcdf
#  
#  (default) module load intel
#  (default) module load cray-mpich
#
#-------

CC=icc
FC=ifort
F90C=ifort
LINK="$F90C -no-ipo"

DEFINES='-DALLOW_USE_MPI -DALWAYS_USE_MPI -DWORDLENGTH=4'
CPP='cpp -traditional -P'
F90FIXEDFORMAT='-fixed -Tf'
EXTENDED_SRC_FLAG='-132'
GET_FC_VERSION="--version"
OMPFLAG='-openmp'

NOOPTFILES='01 -fp-model precise -g -xCORE-AVX2'

FFLAGS="$FFLAGS -W0 -WB -convert big_endian -assume byterecl"
FFLAGS="$FFLAGS -fPIC"
FFLAGS="$FFLAGS -mcmodel=large -shared-intel"
#- might want to use '-r8' for fizhi pkg:
#FFLAGS="$FFLAGS -r8"

FOPTIM='-O2 -align -traceback -xCORE-AVX2 -ip -ftz -fp-model precise'
NOOPTFILES='seaice_growth.F calc_oce_mxlayer.F fizhi_lsm.F fizhi_clockstuff.F'

F90FLAGS=$FFLAGS
F90OPTIM=$FOPTIM
CFLAGS='-O0 -ip -fPIC'

# Test if MPICH is set
if [ -z ${MPICH_DIR} ] ; then
	echo "MPICH unset ... "
else

        INCLUDEDIRS="/opt/cray/pe/mpt/7.7.3/gni/mpich-intel/16.0/include /opt/cray/xpmem/default/include /opt/cray/ugni/default/include /opt/cray/udreg/default/include /opt/cray/dmapp/default/include /opt/cray/pe/pmi/default/include"
        INCLUDES="-I/opt/cray/pe/mpt/7.7.3/gni/mpich-intel/16.0/include -I/opt/cray/xpmem/default/include -I/opt/cray/ugni/default/include -I/opt/cray/udreg/default/include -I/opt/cray/dmapp/default/include -I/opt/cray/pe/pmi/default/include"
        LIBS="-L/opt/cray/xpmem/default/lib64 -L/opt/cray/ugni/default/lib64 -L/opt/cray/udreg/default/lib64 -L/opt/cray/pe/pmi/default/lib64 -L/opt/cray/dmapp/default/lib64 -L/opt/cray/pe/mpt/7.7.3/gni/mpich-intel/16.0/lib -ldl -lmpichf90_intel -lmpich_intel -lrt -lugni -lpmi -ldl -lxpmem -lpthread -ludreg"
	MPIINCLUDEDIR="${MPICH_DIR}"
fi

# Now test for netcdf 
if [ -z ${TACC_NETCDF_INC} ] ; then
	echo "NETCDF unset ... "	
else
	INCLUDEDIRS="${INCLUDEDIRS} ${TACC_NETCDF_INC}"
	INCLUDES="${INCLUDES} -I${TACC_NETCDF_INC}"
	LIBS="${LIBS} -L${TACC_NETCDF_LIB} -lnetcdf -lnetcdff"
fi
